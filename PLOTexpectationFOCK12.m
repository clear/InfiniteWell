
M1=dlmread('CoefficentsC2.txt');
Mc1=[complex(M1(:,1),M1(:,2)),complex(M1(:,3),M1(:,4))];
Mc2=[complex(M1(:,5),M1(:,6)),complex(M1(:,7),M1(:,8))];

L=94.486299429; %units in a.u.
n=2; %basis size
M=500; %time steps
y1=zeros(M,1);
y2=zeros(M,1);

%expectation integral 
syms int(x)
int1(x)=((x^2)/4)-cos(2*pi*x/L)/(8*(pi/L)^2)-x*sin(2*pi*x/L)/(4*pi/L);
int2(x)=((x^2)/4)-cos(4*pi*x/L)/(8*(2*pi/L)^2)-x*sin(4*pi*x/L)/(8*pi/L);
int3(x)=(1/2)*((x*sin(-x*pi/L)/(-pi/L))-(x*sin(x*3*pi/L)/(3*pi/L))+(cos(-x*pi/L)/(-pi/L)^2)-(cos(x*3*pi/L)/(3*pi/L)^2));
for t=1:M
    y1(t)=(2/L)*(Mc1(t,1)*conj(Mc1(t,1))*(int1(L)-int1(0))+Mc1(t,2)*conj(Mc1(t,2))*(int2(L)-int2(0))+Mc1(t,1)*conj(Mc1(t,2))*(int3(L)-int3(0))+Mc1(t,2)*conj(Mc1(t,1))*(int3(L)-int3(0)));
    y2(t)=(2/L)*(Mc2(t,1)*conj(Mc2(t,1))*(int1(L)-int1(0))+Mc2(t,2)*conj(Mc2(t,2))*(int2(L)-int2(0))+Mc2(t,1)*conj(Mc2(t,2))*(int3(L)-int3(0))+Mc2(t,2)*conj(Mc2(t,1))*(int3(L)-int3(0)));
end

%plot expectation value
figure 
t=linspace(1,M,M);
y1=y1sum/18.897259886; %a.u. -> nm
y2=y2sum/18.897259886; %a.u. -> nm
meanE1=mean(y1)
meanE2=mean(y2)
plot(t,y1,t,y2)
legend('E1','E2','Location','northeast');
legend('boxoff');    
ylabel('<x> (nm)');
xlabel('time step');

%xFF=fopen('xFF.txt','w');
%fprintf(xFF,'%f32 ', t);
%yFFE1=fopen('yFFE1.txt','w');
%fprintf(yFFE1,'%f32 ', y1);
%yFFE2=fopen('yFFE2.txt','w');
%fprintf(yFFE2,'%f32 ', y2);
%yCOE1=fopen('yCOE1.txt','w');
%fprintf(yCOE1,'%f32 ', y1);
%yCOE2=fopen('yCOE2.txt','w');
%fprintf(yCOE2,'%f32 ', y2);
%yEOE1=fopen('yEOE1.txt','w');
%fprintf(yEOE1,'%f32 ', y1);
%yEOE2=fopen('yEOE2.txt','w');
%fprintf(yEOE2,'%f32 ', y2);


