
M1=dlmread('CoefficentsC6.txt');
b=6; % basis size
N=100001; % time steps ; (well size is 5e-9); L=94.486299429; %units in a.u.
Mc1=[complex(M1(:,1),M1(:,2)),complex(M1(:,3),M1(:,4)),complex(M1(:,5),M1(:,6)),complex(M1(:,7),M1(:,8)),complex(M1(:,9),M1(:,10)),complex(M1(:,11),M1(:,12))];
Mc2=[complex(M1(:,13),M1(:,14)),complex(M1(:,15),M1(:,16)),complex(M1(:,17),M1(:,18)),complex(M1(:,19),M1(:,20)),complex(M1(:,21),M1(:,22)),complex(M1(:,23),M1(:,24))];
y1=zeros(N,1);
y2=zeros(N,1);

%Correlation function
for t=1:N
    for i=1:b
y1(t)=y1(t)+(1/N)*Mc1(1,i)*conj(Mc1(t,i));
y2(t)=y2(t)+(1/N)*Mc2(1,i)*conj(Mc2(t,i));
    end
end

%Fourier transform
ya=abs(fftshift(fft(y1)));
yb=abs(fftshift(fft(y2)));

%plot correlation function
figure 
t=linspace(0,N-1,N);
subplot(2,1,1)
plot(t,y1,'c',t,y2,'m');
legend('boxoff');
title('Correlation function');    
ylabel('Re C(t)');
xlabel('time steps');

dt=4.1456; %time step size
f=27.21138602*2*pi*(-N/2:(N/2)-1)/(N*dt); %sets x-axis to eV from Hartree

f1=f(50002:52500)
ya1=ya(50002:52500)
yb1=yb(50002:52500)

%plot energy spectrum
subplot(2,1,2)
plot(f,ya,f,yb);
legend('boxoff');
title('FT');    

%xFT=fopen('xFT.txt','w');
%fprintf(xFT,'%f32 ', f1');
%yFT1=fopen('yFT1.txt','w');
%fprintf(yFT1,'%f32 ', ya1);
%yFT2=fopen('yFT2.txt','w');
%fprintf(yFT2,'%f32 ', yb1);
