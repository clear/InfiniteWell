!2 electrons in an infinite well//2 inital orthogonal coefficients for each electron//Time evolution of inital coeffcients//J and K integrals found from mathematica
program main
implicit none
integer :: a,i
include '2ELECfockC2data.txt'
integer(kind = kind(1.0d0)), parameter :: n=2,Elec=2
complex(kind = kind(1.0d0)), dimension (n,n)::F1,Fp,F2
complex(kind = kind(1.0d0)),dimension(n*Elec) :: C1,Cp,C2

!INITAL COEFFICIENTS
open(unit=91,file="inital2coeff.txt",status="old")
 do i=1,Elec*n
  read(91,*)C1(i)
 end do
close(91)
open(unit=96,file="CoefficentsC2.txt",form="formatted",status="unknown",position="append")
 write(96,5)C1
  5 format(4(2x,f33.30,f33.30)) ! 4=n*Elec
close(96)
!TIME EVOLUTION
do i=1,nn
if (i>1) then
 do a=1,Elec*n
  C1(a)=C2(a)
 end do
end if
call FOCK(C1,F1) !build inital fock matrix
call UNITARYEVO(F1,C1,Cp) !time evolve inital coeffs to obtian predicted coeffs
call FOCK(Cp,Fp) !build predicted fock matrix 
 F2=(F1+Fp)*0.5 !average fock matrices
call UNITARYEVO(F2,C1,C2) !time evolve inital coeffs with averaged fock matrix
open(unit=96,file="CoefficentsC2.txt",form="formatted",status="unknown",position="append")
 write(96,5)C2
close(96)
end do
end program main

subroutine FOCK(C1,F)
implicit none
integer :: a,b,c,d
include '2ELECfockC2data.txt'
real(kind=kind(1.0d0)), parameter :: pi=4.d0*atan(1.d0)
integer, parameter::n=2,Elec=2
real(kind=kind(1.0d0)), dimension (n,n,n,n)::K,J,G
real(kind=kind(1.0d0)), dimension (n,n) :: h
complex(kind = kind(1.0d0)), dimension (n,n), intent(out) :: F
complex(kind = kind(1.0d0)), dimension(n,n)::P,V
complex(kind = kind(1.0d0)),dimension (Elec*n),intent(in) :: C1
!Density function
do c=1,n
 do d=1,n
   P(c,d)=CONJG(C1(c))*C1(d)
  do a=1,Elec-1
   P(c,d)=P(c,d)+CONJG(C1(c+a*n))*C1(d+a*n)
  end do
 end do
end do
!KINETIC TERM
do a=1,n
 do b=1,n
  h(a,b)=0
 end do
  h(a,a)=(a**2*pi**2/(2*L**2))
end do
!COULOMB INTEGRAL MATRIX (nxnxnxn)
!J=reshape((/0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0/),(/n,n,n,n/))
open(unit=88,file="jmatrix2C.txt",status="old")
read(88,*)((((J(a,b,c,d),a=1,n),b=1,n),c=1,n),d=1,n)
close(88)
!EXCHANGE INTEGRAL MATRIX (nxnxnxn)
K=reshape((/0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0/),(/n,n,n,n/))
!open(unit=88,file="kmatrix2C.txt",status="old")
!read(88,*)((((K(a,b,c,d),a=1,n),b=1,n),c=1,n),d=1,n)
!close(88)
!G MATRIX (nxnxnxn)
do a=1,n
 do b=1,n
  do c=1,n
   do d=1,n
    G(a,b,c,d)=2*J(a,b,c,d)-K(a,b,c,d)
   end do
  end do
 end do
end do
!Builds Fock matrix
do a=1,n !sets V to a matrix of zeros
 do b=1,n
  V(a,b)=0
 end do
end do
do a=1,n
 do b=1,n
  do c=1,n
   do d=1,n
    V(a,b)=P(c,d)*G(a,b,c,d)+V(a,b)
   end do
  end do
 end do
end do
do a=1,n
 do b=1,n
  F(a,b)=h(a,b)+V(a,b)
 end do
end do
end subroutine FOCK

subroutine UNITARYEVO(F,Ci,Cf)
implicit none
integer :: a,b,c,info
integer, parameter::n=2,Elec=2
include '2ELECfockC2data.txt'
complex(kind = kind(1.0d0)), dimension (n,n)::U,Nu,X,De,Deinv,S
complex(kind = kind(1.0d0)), dimension (n,n),intent(in)::F
complex(kind = kind(1.0d0)),parameter :: Z=(0,1)
complex(kind = kind(1.0d0)),dimension(Elec*n), intent(in) :: Ci
complex(kind = kind(1.0d0)),dimension(Elec*n), intent(out) :: Cf
integer, dimension(size(F,1))::ipiv,work
!INDENITY MATRIX
do a=1,n
 do b=1,n
  S(a,b)=0
 end do
  S(a,a)=1
end do
!Unitary matrix
X=Z*matmul(S, F)
Nu=S-X*(t/2.0)
De=S+X*(t/2.0)
Deinv=De
call ZGETRF(n,n,Deinv,n,ipiv,info)!! to get LU matrix !!
call ZGETRI(n,Deinv,n,ipiv,work,n*n,info)!! to get the inverse matrix
U=matmul(Nu, Deinv)
!TIME EVOLVED COEFFS
do a=1,n*Elec
 Cf(a)=0
end do
do c=0,Elec-1
 do a=1,n
  do b=1,n
   Cf(a+c*n)=U(a,b)*Ci(b+c*n)+Cf(a+c*n) 
  end do
 end do
end do
end subroutine UNITARYEVO
