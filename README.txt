TIME EVOLUTION CODE FOR TWO ELECTRONS IN AN INFINITE WELL.
Initial state for both electrons approximated as finite linear combination of the stationary energy eigenfunction with a set of orthogonal time dependent coefficients.
Fortran90 code time evolves initial state with unitary time evolution operator. 
Matlab code calculates Fourier transform of the time correlation function for both electron wavefunctions.

----- Files for 6 initial coefficients -----

inital6coeff.txt --- Files containing the initial coefficients for electron 1 and 2.

2ELECfock6Cdata.txt ---  Parameter file for 2ELECfock6C.f90, L=well width, t=time step size, nn=number of time steps.

jmatrix.txt, kmatrix.txt --- text files of coulomb and exchange integrals computed from mathematica matrix dimensions (6x6x6x6)

2ELECfock6c.f90 --- Fortran90 file: time evolves initial coefficients from input file for nn steps, prints coefficients into output file.

PLOTelec6coeffCorrelationfunction.m --- Matlab file: plots the correlation function and Fourier transform by reading in the Coefficient files obtained from running time evolution f90 code. 

----- Files for 2 initial coefficients -----

all files with  '2' in the name are equivalent to above but specialized for 2 initial coefficients.

PLOTexpectationFOCK12.m --- Matlab file: plots expectation value of position in the well at each time step.

