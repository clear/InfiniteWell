!!2 electrons in an infinite well//6 inital orthogonal coefficients for each electron//Time evolution of inital coeffcients//J and K integrals found from mathematica

program main
implicit none
integer :: a,i
integer, parameter :: n=6,Elec=2
include '2ELECfock6Cdata.txt'
complex(kind = kind(1.0d0)), dimension (n,n)::F1,Fp,F2
complex(kind = kind(1.0d0)),dimension(Elec*n) :: C1,Cp,C2
!INITAL COEFFS
open(unit=91,file="inital6coeff.txt",status="old")
do i=1,Elec*n
read(91,*)C1(i)
end do
close(91)
open(unit=96,file="CoefficentsC6.txt",form="formatted",status="unknown",position="append")
write(96,5)C1
5 format(12(2x,f33.30,f33.30)) !! 12=n*Elec
close(96)
do i=1,nn
!TIME EVOLUTION
if (i>1) then
 do a=1,Elec*n
  C1(a)=C2(a)
 end do
end if
call FOCK(C1,F1) !build inital fock matrix
call UNITARYEVO(F1,C1,Cp) !time evolve coeffs to obtian predicted coeffs
call FOCK(Cp,Fp) !build predicted fock matrix
F2=(F1+Fp)*0.5 !average fock matrices
call UNITARYEVO(F2,C1,C2) !time evolve inital coeffs with averaged fock matrix 
open(unit=96,file="CoefficentsC6.txt",form="formatted",status="unknown",position="append")
write(96,5)C2
close(96)
end do
end program main

subroutine FOCK(C1,F)
implicit none
integer :: a,b,c,d
integer, parameter::n=6,Elec=2
real(kind=kind(1.0d0)), dimension (n,n):: h
include '2ELECfock6Cdata.txt'
complex(kind = kind(1.0d0)), dimension (n,n), intent(out) :: F
complex(kind = kind(1.0d0)), dimension(n,n)::P,V
complex(kind = kind(1.0d0)),dimension (Elec*n),intent(in) :: C1
real(kind=kind(1.0d0)),dimension (n,n,n,n) ::G,J,K
real(kind=kind(1.0d0)), parameter :: pi=4.d0*atan(1.d0)
!DENSITY MATRIX
do c=1,n
 do d=1,n
   P(c,d)=CONJG(C1(c))*C1(d)
  do a=1,Elec-1
   P(c,d)=P(c,d)+CONJG(C1(c+a*n))*C1(d+a*n)
  end do
 end do
end do
!KINETIC TERM
do a=1,n
 do b=1,n
  h(a,b)=0
 end do
 h(a,a)=(a**2*pi**2/(2*L**2))
end do
!COULOMB INTEGRAL MATRIX (2x2x2x2)
open(unit=89,file="jmatrix.txt",status="old")
read(89,*)((((J(a,b,c,d),a=1,n),b=1,n),c=1,n),d=1,n)
close(89)
!do a=1,n
! do b=1,n
!  do c=1,n
!   do d=1,n
!    J(a,b,c,d)=0.0
!   end do
!  end do
! end do
!end do
!EXCHANGE INTEGRAL MATRIX (2x2x2x2)
open(unit=88,file="kmatrix.txt",status="old")
read(88,*)((((K(a,b,c,d),a=1,n),b=1,n),c=1,n),d=1,n)
close(88)
!do a=1,n
! do b=1,n
!  do c=1,n
!   do d=1,n
!    K(a,b,c,d)=0.0
!   end do
!  end do
! end do
!end do
!G MATRIX (2x2x2x2)
do a=1,n
 do b=1,n
  do c=1,n
   do d=1,n
    G(a,b,c,d)=2*J(a,b,c,d)-K(a,b,c,d)
   end do
  end do
 end do
end do
!Builds Fock matrix
do a=1,n
 do b=1,n
  V(a,b)=0
 end do
end do
do a=1,n
 do b=1,n
  do c=1,n
   do d=1,n
    V(a,b)=P(c,d)*G(a,b,c,d)+V(a,b)
   end do
  end do
 end do
end do
do a=1,n
 do b=1,n
  F(a,b)=h(a,b)+V(a,b)
 end do
end do
end subroutine FOCK

subroutine UNITARYEVO(F,Ci,Cf)
implicit none
integer :: a,b,c,info
integer, parameter::n=6,Elec=2
include '2ELECfock6Cdata.txt'
complex(kind = kind(1.0d0)), dimension (n,n)::U,Nu,X,De,Deinv,S
complex(kind = kind(1.0d0)), dimension (n,n) ::F
complex(kind = kind(1.0d0)),parameter :: Z=(0,1)
complex(kind = kind(1.0d0)),dimension(Elec*n), intent(in) :: Ci
complex(kind = kind(1.0d0)),dimension(Elec*n), intent(out) :: Cf
integer, dimension(size(F,1))::ipiv,work
!!IDENITY MATRIX
do a=1,n
 do b=1,n
  S(a,b)=0
 end do
  S(a,a)=1
end do
!UNITARY MATRIX
X=Z*matmul(S, F)
Nu=S-X*(t/2.0)
De=S+X*(t/2.0) 
Deinv=De
call ZGETRF(n,n,Deinv,n,ipiv,info)!! to get LU matrix !!
call ZGETRI(n,Deinv,n,ipiv,work,n*n,info)!! to get the inverse matrix
U=matmul(Nu, Deinv)
!TIME EVOLVED COEFFCICIENTS
do a=1,n*Elec
 Cf(a)=0
end do
do c=0,Elec-1
 do a=1,n
  do b=1,n
   Cf(a+c*n)=U(a,b)*Ci(b+c*n)+Cf(a+c*n) 
  end do
 end do
end do
end subroutine UNITARYEVO

